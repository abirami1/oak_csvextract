﻿using LumenWorks.Framework.IO.Csv;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;

namespace oak_csvextract
{
    class Program
    {
        static string cur_date = DateTime.Now.ToString("yyyy-MM-dd");
        private static readonly HttpClient client = new HttpClient();
        static void Main(string[] args)
        {
            string connstring = "Server = localhost; Database = factory; Uid = root; Pwd = ;SslMode=none";
            MySqlConnection mcon = new MySqlConnection(connstring);
            mcon.Open();
            string connstring_setupdb = "Server = localhost; Database = setupsheetdb; Uid = root; Pwd = ;SslMode=none";
            MySqlConnection mcon_setup = new MySqlConnection(connstring_setupdb);
            mcon_setup.Open();
            var files = Directory.GetFiles(@"F:\xampp\htdocs\alfadockpro\factory_layout\493\RPA", "*.csv");
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Log("Count:" + files.Length, w);
            }
            foreach (String f in files)
            {
                string fname = System.IO.Path.GetFileNameWithoutExtension(f);

                if (fname.Contains("Processing"))
                {
                    extract_processdata(f, mcon, mcon_setup);
                }
                else if (fname.Contains("Status"))
                {
                    extract_timedata(f, mcon);
                }
            }

        }
        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            w.WriteLine("  :");
            w.WriteLine($"  :{logMessage}");
            w.WriteLine("-------------------------------");
        }

        public static void DumpLog(StreamReader r)
        {
            string line;
            while ((line = r.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
        }
        public static void extract_timedata(string path, MySqlConnection mcon)
        {
            string fname = System.IO.Path.GetFileNameWithoutExtension(path);
            string[] fname1 = fname.Split('_');

            using (CsvReader csv =
                            new CsvReader(new StreamReader(path), true))
            {
                while (csv.ReadNextRecord())
                {
                    string machinename = fname1[0];
                    if (machinename.Contains("8025"))
                        machinename = "HG8025";
                    string date = csv[0];
                    string date1 = date.Replace("/", "-");
                    string date2 = date.Replace("-", "");
                    double poweroffsecs = Convert.ToDouble(csv[1]);
                    double standbysecs = Convert.ToDouble(csv[2]);
                    double setupsecs = Convert.ToDouble(csv[3]);
                    double opsecs = Convert.ToDouble(csv[4]);
                    double alarmsecs = Convert.ToDouble(csv[5]);
                    string slots = csv[6];
                    string CmdText = "INSERT IGNORE INTO oak_csvtime VALUES(@idcsv,@machname,@date,@poweroffsecs,@standbysecs,@setupsecs,@opsecs,@alarmsecs,@slots)";
                    MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                    cmd.Parameters.AddWithValue("@idcsv", date1 + "-" + fname1[0]);
                    cmd.Parameters.AddWithValue("@machname", machinename);
                    cmd.Parameters.AddWithValue("@date", date1);
                    cmd.Parameters.AddWithValue("@poweroffsecs", poweroffsecs);
                    cmd.Parameters.AddWithValue("@standbysecs", standbysecs);
                    cmd.Parameters.AddWithValue("@setupsecs", setupsecs);
                    cmd.Parameters.AddWithValue("@opsecs", opsecs);
                    cmd.Parameters.AddWithValue("@alarmsecs", alarmsecs);
                    cmd.Parameters.AddWithValue("@slots", slots);
                    int result = cmd.ExecuteNonQuery();
                    if (result != 1)
                    {
                        string query = string.Format(@"UPDATE oak_csvtime SET poweroffsecs={1} , standbysecs={2}, setupsecs={3}, opsecs={4}, alarmsecs={5} where  idcsv='{0}'", date1 + "-" + fname1[0], poweroffsecs, standbysecs, setupsecs, opsecs, alarmsecs);


                        using (MySqlCommand command = new MySqlCommand(query, mcon))
                        {
                            int row = command.ExecuteNonQuery();
                        }
                    }
                }
            }
        }

        public static void updateOakpgminfo(string partno, string sdate, string edate, MySqlConnection mcon)
        {

            try
            {
               sdate = sdate.Replace("/", "-");
                edate = edate.Replace("/", "-");
                string query = string.Format(@"UPDATE `setupsheetdb`.setupsheet SET processed=2,starttime='{1}',endtime='{2}'  where compid='493' AND programname='{0}' AND (starttime='0000-00-00 00:00:00' OR endtime='0000-00-00 00:00:00') AND processed!=2", partno, sdate, edate);

                using (MySqlCommand command = new MySqlCommand(query, mcon))
                {
                    int row = command.ExecuteNonQuery();
                    if (row == 1)
                    {

                        using (StreamWriter w = File.AppendText("log.txt"))
                        {
                            Log("Success: " + partno, w);
                        }
                    }
                }
                

            }
            catch (Exception)
            {
            }
            finally
            {

            }


        }

        async void update_GPN(String partno, string pid, string qt, string userid, bool str_datefilter, string orderno, string start_date, string end_date, string due_date)
        {
          /*  var str_arg = "{\"partNo\":\"" + partno + "\",\"processid\":\"" + pid + "\",\"status\":\"2\",\"quantity\":\"" + qt + "\",\"userid\":\"" + userid + "\",\"isSearchApplyDateFilters\":\"false\",\"setGivenTime\":\"true\",\"orderNo\":\"" + orderno + "\",\"startdate\":\"" + start_date + "\",\"enddate\":\"" + end_date + "\",\"dueDate\":\"" + due_date + "\"}";

            var values = new Dictionary<string, string>
{
    { "plugin", "SchedulerAPI" },
    { "controller", "SchedulerSubmitProcessController" },
    { "action", "SiProcessForLoadBalance" },
    { "args", str_arg }
};

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("http://13.115.237.181/api/plugin", content);

            //var responseString = 
            await response.Content.ReadAsStringAsync();
            */
        }
        public static void extract_processdata(string path, MySqlConnection mcon, MySqlConnection mcon2)
        {
            try
            {
                Program pg = new Program();
                string fname = System.IO.Path.GetFileNameWithoutExtension(path);
                string[] fname1 = fname.Split('_');
                string starttime = "";
                string endtime = "";
                string machname = ""; string pgno = "";
                using (CsvReader csv =
                            new CsvReader(new StreamReader(path), true))
                {
                    int counter = 1;
                    int fieldCount = csv.FieldCount;
                    string[] headers = csv.GetFieldHeaders();
                    while (csv.ReadNextRecord())
                    {
                        if (counter > 1 && csv[0].Equals("S"))
                        {
                            string recordtype = csv[0];
                            string sheetID = csv[1];
                            machname = csv[2];
                            string pg_machname = csv[3];
                            //
                            pgno = csv[4];
                            string pgcomment = csv[5];
                            starttime = csv[6];
                            DateTime Date = DateTime.Parse(starttime);

                            endtime = csv[7];
                            DateTime Date1 = DateTime.Parse(endtime);

                            string actqty = csv[8];
                            string defect = csv[9];
                            string schname = csv[10];
                            string machopmode = csv[11];
                            string axisspeed = csv[12];
                            string matname = csv[13];
                            string lasermatname = csv[14];
                            string dimx = csv[15];
                            string dimy = csv[16];
                            string machtime = csv[18];

                            TimeSpan prtime = TimeSpan.Parse(machtime);
                            double ProcTimeSecs = prtime.TotalSeconds;
                            string punchtime = csv[19];
                            string lasertime = csv[20];
                            string setuptime = csv[21];
                            string alarmtime = csv[22];

                            TimeSpan AlarmTime = TimeSpan.Parse(alarmtime);
                            double AlarmTimeSecs = AlarmTime.TotalSeconds;
                            string alarmcontent = csv[23];
                            /*
                            string matcode = csv[13];

                           
                            string schqty = csv[17];

                            string actprocesstime = csv[19];

                           

                            string diecount = csv[25];
                            string yieldrate = csv[26];
                            string hits = csv[27];
                            string completed = csv[28];
                            string deldate = csv[30];
                            */
                            string CmdText = "INSERT IGNORE INTO oak_csvextract VALUES(@idcsv,@recordtype,@sheetid,@machname,@pmmachname,@schedulename,@progno,@progcomment,@axisspeed,@materialcode,@materialname,@lasermatname,@matx,@maty,@machiningtime,@starttime,@endtime,@schqty,@actqty,@actprocesstime,@alarmtime,@alarmcontent,@setuptime,@punchtime,@lasertime,@diecount,@yield,@hits,@completed,@opmode,@deldate,@defect,@status,@prosecs,@alarmsecs)";
                            MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                            cmd.Parameters.AddWithValue("@idcsv", sheetID + "-" + pgno);
                            cmd.Parameters.AddWithValue("@recordtype", recordtype);
                            cmd.Parameters.AddWithValue("@sheetid", sheetID);
                            cmd.Parameters.AddWithValue("@machname", fname1[0]);
                            cmd.Parameters.AddWithValue("@pmmachname", machname);
                            cmd.Parameters.AddWithValue("@schedulename", schname);
                            cmd.Parameters.AddWithValue("@progno", pgno);
                            cmd.Parameters.AddWithValue("@progcomment", Encoding.UTF8.GetBytes(pgcomment));
                            cmd.Parameters.AddWithValue("@axisspeed", axisspeed);
                            cmd.Parameters.AddWithValue("@materialcode", "");
                            cmd.Parameters.AddWithValue("@materialname", matname);
                            cmd.Parameters.AddWithValue("@lasermatname", lasermatname);
                            cmd.Parameters.AddWithValue("@matx", dimx);
                            cmd.Parameters.AddWithValue("@maty", dimy);
                            cmd.Parameters.AddWithValue("@machiningtime", machtime);
                            cmd.Parameters.AddWithValue("@starttime", starttime);
                            cmd.Parameters.AddWithValue("@endtime", endtime);
                            cmd.Parameters.AddWithValue("@schqty", "");
                            cmd.Parameters.AddWithValue("@actqty", actqty);
                            cmd.Parameters.AddWithValue("@actprocesstime", "");
                            cmd.Parameters.AddWithValue("@alarmtime", alarmtime);
                            cmd.Parameters.AddWithValue("@alarmcontent", alarmcontent);
                            cmd.Parameters.AddWithValue("@setuptime", setuptime);
                            cmd.Parameters.AddWithValue("@punchtime", punchtime);
                            cmd.Parameters.AddWithValue("@lasertime", lasertime);
                            cmd.Parameters.AddWithValue("@diecount", "");
                            cmd.Parameters.AddWithValue("@yield", "");
                            cmd.Parameters.AddWithValue("@hits", "");
                            cmd.Parameters.AddWithValue("@completed", "");
                            cmd.Parameters.AddWithValue("@opmode", machopmode);
                            cmd.Parameters.AddWithValue("@deldate", "");
                            cmd.Parameters.AddWithValue("@defect", defect);
                            cmd.Parameters.AddWithValue("@status", "0");
                            cmd.Parameters.AddWithValue("@prosecs", ProcTimeSecs);
                            cmd.Parameters.AddWithValue("@alarmsecs", AlarmTimeSecs);

                            int result = cmd.ExecuteNonQuery();
                            int def = Convert.ToInt32(defect);
                            if (def == 0)
                            {
                                updateOakpgminfo(pgno, starttime, endtime, mcon2);
                            }


                        }
                        else if (counter > 1 && csv[0].Equals("P"))
                        {
                            string recordtype = csv[0];
                            //string sheetID = csv[1];

                            string partno = csv[1];
                            string prodno = csv[2];
                            string machtime = csv[3];
                            string punchtime = csv[4];
                            string dimx = csv[5];
                            string dimy = csv[6];
                            string partqty = csv[7];
                           // string partcomment = csv[7];
                            //string lasertime = csv[10];


                            string CmdText = "INSERT IGNORE INTO oak_csvextract VALUES(@idcsv,@recordtype,@sheetid,@machname,@pmmachname,@schedulename,@progno,@progcomment,@axisspeed,@materialcode,@materialname,@lasermatname,@matx,@maty,@machiningtime,@starttime,@endtime,@schqty,@actqty,@actprocesstime,@alarmtime,@alarmcontent,@setuptime,@punchtime,@lasertime,@diecount,@yield,@hits,@completed,@opmode,@deldate,@defect,@status,@prosecs,@alarmsecs)";
                            MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                            cmd.Parameters.AddWithValue("@idcsv", prodno + "-" + partno);
                            cmd.Parameters.AddWithValue("@recordtype", recordtype);
                            cmd.Parameters.AddWithValue("@sheetid", "");
                            cmd.Parameters.AddWithValue("@machname", fname1[0]);
                            cmd.Parameters.AddWithValue("@pmmachname", "");
                            cmd.Parameters.AddWithValue("@schedulename", prodno);
                            cmd.Parameters.AddWithValue("@progno", partno);
                            cmd.Parameters.AddWithValue("@progcomment", Encoding.UTF8.GetBytes(""));
                            cmd.Parameters.AddWithValue("@axisspeed", "");
                            cmd.Parameters.AddWithValue("@materialcode", "");
                            cmd.Parameters.AddWithValue("@materialname", "");
                            cmd.Parameters.AddWithValue("@lasermatname", "");
                            cmd.Parameters.AddWithValue("@matx", dimx);
                            cmd.Parameters.AddWithValue("@maty", dimy);
                            cmd.Parameters.AddWithValue("@machiningtime", machtime);
                            cmd.Parameters.AddWithValue("@starttime", "");
                            cmd.Parameters.AddWithValue("@endtime", "");
                            cmd.Parameters.AddWithValue("@schqty", "");
                            cmd.Parameters.AddWithValue("@actqty", partqty);
                            cmd.Parameters.AddWithValue("@actprocesstime", "");
                            cmd.Parameters.AddWithValue("@alarmtime", "");
                            cmd.Parameters.AddWithValue("@alarmcontent", "");
                            cmd.Parameters.AddWithValue("@setuptime", "");
                            cmd.Parameters.AddWithValue("@punchtime", punchtime);
                            cmd.Parameters.AddWithValue("@lasertime", "");
                            cmd.Parameters.AddWithValue("@diecount", "");
                            cmd.Parameters.AddWithValue("@yield", "");
                            cmd.Parameters.AddWithValue("@hits", "");
                            cmd.Parameters.AddWithValue("@completed", "");
                            cmd.Parameters.AddWithValue("@opmode", "");
                            cmd.Parameters.AddWithValue("@deldate", "");
                            cmd.Parameters.AddWithValue("@defect", "");
                            cmd.Parameters.AddWithValue("@status", "0");
                            cmd.Parameters.AddWithValue("@prosecs", 0);
                            cmd.Parameters.AddWithValue("@alarmsecs", 0);

                            int result = cmd.ExecuteNonQuery();

                            if (result == 1)
                            {
                                // pg.update_GPN(partno, "8075", partqty, "1822", false, "-1", starttime.Replace("/", "-"), endtime.Replace("/", "-"), cur_date);

                                // using (StreamWriter w = File.AppendText("log.txt"))
                                // {
                                //    Log("part_no:" + partno + partqty + starttime.Replace("/", "-") + starttime.Replace("/", "-"), w);
                                //  }
                            }

                        }
                        else if (fname1[0].StartsWith("EG") || fname1[0].Contains("8025"))
                        {
                            string processingmacname = csv[0];
                            string partname = csv[2];
                            string partcomment = csv[3];
                            string machinename = fname1[0];
                            if (machinename.Contains("8025"))
                                machinename = "HG8025";
                            string starttime2 = csv[4];
                            DateTime Procstarttime = DateTime.Parse(starttime2);
                            string endtime2 = csv[5];
                            DateTime Procendtime = DateTime.Parse(endtime2);

                            TimeSpan ProcTime = Procendtime - Procstarttime;
                            double ProcTimeSecs = ProcTime.TotalSeconds;

                            string processno = csv[6];
                            string defno = csv[7];
                            string lenx = csv[8];
                            string leny = csv[9];
                            string matname = csv[10];
                            string sheetthickness = csv[11];
                            string newrep = csv[13];
                            string punchno = csv[15];
                            string dieno = csv[16];
                            string bendlength = csv[17];
                            string nosetup = csv[19];
                            string startcount = csv[21];
                            string trialtime = csv[30];
                            string machiningtime = csv[31];
                            TimeSpan machTime = TimeSpan.Parse(machiningtime);
                            double MachTimeSecs = machTime.TotalSeconds;
                            string displaytime = csv[33];
                            
                            string alarmtime = csv[35];

                            TimeSpan AlarmTime = TimeSpan.Parse(alarmtime);
                            double AlarmTimeSecs = AlarmTime.TotalSeconds;

                           

                            /*string machiningno = csv[15];
                           string actmachining = csv[16];


                          string actprocesstime = csv[18];
                           string shotno = csv[20];
                           string shotinch = csv[21];
                           string shotaction = csv[22];
                           string shotq = csv[23];
                           string shotbar = csv[24];
                           string shotmold = csv[25];

                           string moldcount = csv[28];
                           string decisiontime = csv[29];
                           string moldtime = csv[30];

                           */

                            string CmdText = "INSERT IGNORE INTO oak_bendcsvextract VALUES(@idcsv,@processingmacname,@partname,@partcomment,@machname,@matname,@sheetthickness,@lenx,@leny,@newrep,@punchno,@dieno,@processno,@bendlength,@nosetup,@starttime,@endtime,@machiningno,@actmachining,@machiningtime,@actprocesstime,@defectno,@shotno,@shotinch,@shotaction,@shotq,@shotbar,@shotmold,@alarmtime,@startcount,@moldcount,@decisiontime,@moldtime,@trialtime,@displaytime,@alarmsecs,@prosecs)";
                            MySqlCommand cmd = new MySqlCommand(CmdText, mcon);

                            cmd.Parameters.AddWithValue("@idcsv", partname + "-" + machiningtime + "-" + counter);
                            cmd.Parameters.AddWithValue("@processingmacname", processingmacname);
                            cmd.Parameters.AddWithValue("@partname", partname);
                            cmd.Parameters.AddWithValue("@partcomment", Encoding.UTF8.GetBytes(partcomment));
                            cmd.Parameters.AddWithValue("@machname", machinename);
                            cmd.Parameters.AddWithValue("@matname", matname);
                            cmd.Parameters.AddWithValue("@sheetthickness", sheetthickness);
                            cmd.Parameters.AddWithValue("@lenx", lenx);
                            cmd.Parameters.AddWithValue("@leny", leny);
                            cmd.Parameters.AddWithValue("@newrep", newrep);
                            cmd.Parameters.AddWithValue("@punchno", punchno);
                            cmd.Parameters.AddWithValue("@dieno", dieno);
                            cmd.Parameters.AddWithValue("@processno", processno);
                            cmd.Parameters.AddWithValue("@bendlength", bendlength);
                            cmd.Parameters.AddWithValue("@nosetup", nosetup);
                            cmd.Parameters.AddWithValue("@starttime", starttime2);
                            cmd.Parameters.AddWithValue("@endtime", endtime2);
                            cmd.Parameters.AddWithValue("@machiningno", "");
                            cmd.Parameters.AddWithValue("@actmachining", "");
                            cmd.Parameters.AddWithValue("@machiningtime", machiningtime);
                            cmd.Parameters.AddWithValue("@actprocesstime", "");
                            cmd.Parameters.AddWithValue("@defectno", defno);
                            cmd.Parameters.AddWithValue("@shotno", "");
                            cmd.Parameters.AddWithValue("@shotinch", "");
                            cmd.Parameters.AddWithValue("@shotaction", "");
                            cmd.Parameters.AddWithValue("@shotq", "");
                            cmd.Parameters.AddWithValue("@shotbar", "");
                            cmd.Parameters.AddWithValue("@shotmold", "");
                            cmd.Parameters.AddWithValue("@alarmtime", alarmtime);
                            cmd.Parameters.AddWithValue("@startcount", startcount);
                            cmd.Parameters.AddWithValue("@moldcount", "");
                            cmd.Parameters.AddWithValue("@decisiontime", "");
                            cmd.Parameters.AddWithValue("@moldtime", "");
                            cmd.Parameters.AddWithValue("@trialtime", "");
                            cmd.Parameters.AddWithValue("@displaytime", displaytime);
                            cmd.Parameters.AddWithValue("@alarmsecs", AlarmTimeSecs);
                            cmd.Parameters.AddWithValue("@prosecs", MachTimeSecs);

                            int result = cmd.ExecuteNonQuery();

                        }

                        counter++;
                    }

                }
            }
            catch (Exception e)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log("Error:" + e, w);
                }
            }
        }
    }
}
